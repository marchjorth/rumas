﻿function showPanel(id){
	var current = $('#panel-'+id);
	$('.panel').removeClass('active');
	current.find('.panel').removeClass('path').removeClass('active');
	current.addClass('active').removeClass('path');
	current.parent('.panel').addClass('path');
	$('.variation').removeClass('active');
}

var panelPositions = [];

function stickyPanels() {
	$('.fullscreen').each(function() {
		panelPositions.push( Math.floor($(this).position().top) );
	});
}

var $carouselMain = $('.carousel-main').flickity({
	cellSelector: '.carousel-cell',
	wrapAround: true,
	imagesLoaded: true,
	adaptiveHeight: true,
	pageDots: false
});

$('.carousel-nav').flickity({
  asNavFor: '.carousel-main',
  imagesLoaded: true,
  contain: true,
  prevNextButtons: false,
  pageDots: false,
  groupCells: 2
});

var $carouselProducts = $('.product-slider .slider-wrapper').flickity({
	cellSelector: '.product',
	autoPlay: 3000,
	wrapAround: true,
	adaptiveHeight: true,	
	prevNextButtons: false,
	pageDots: false
});

$(document).ready(function() {

	$('.download-list input[type=radio]').on('change', function() {
		var value = $(this).val();
		$('.download-list .download').addClass('hide');
		$('.download-list .download').each(function() {
			if( $(this).data('type').indexOf(value) >= 0 ) {
				$(this).removeClass('hide');
			}
		});
	});

/*	$carouselMain.on( 'staticClick.flickity', function( event, pointer, cellElement, cellIndex ) {
		console.log( cellElement.dataset.url );
	}); */

	$('.nav-link').on('click', function(e) {
		e.preventDefault();
		var href = $(this).attr('href');
		var panel = href.substring(href.indexOf("#")+1);
		showPanel(panel);
		$('.navbar-nav .nav-link').removeClass('active');
		$(this).addClass('active');
	});

	$('.menu-toggle').on('click', function() {
		$('body').toggleClass('menu-active');

		$(this).children('i.fa').toggleClass('fa-times').toggleClass('fa-bars');
	});
	$('.menu-toggle').hover(
		function() {
			$('body').addClass('menu-hover');
		}, function() {
	    	$('body').removeClass('menu-hover');
	});
	$('.nav-overlay').on('click', function() {
		$('body').removeClass('menu-active');
		$('.menu-toggle i.fa').removeClass('fa-times').addClass('fa-bars');
	});

	lightbox.option({
      'resizeDuration': 200,
      'albumLabel': "%1 af %2",
      'alwaysShowNavOnTouchDevices': true,
      'fitImagesInViewport': true,
      'wrapAround': true
    })

	$('.menu-search').click(function() {
		$(this).addClass('active');
	});
	$('.menu-search input').focusout(function() {
		if ($(this).val().length <= 0){
			$('.menu-search').removeClass('active');
		}
	});
	$('.menu-search .fa').click(function() {
		if ($('.menu-search input[name=q]').val().length > 0){
			$('.menu-search').submit();
		}
	});

	$('.fullscreen .scroll-indicator').click(function() {
		var panel = $(this).parent('.fullscreen');
		var location = panel.position().top + panel.innerHeight();
		$('html, body').animate({
        	scrollTop: location
	    }, 500);
	});

	if (typeof variationsJson != "undefined") {
	/*	$('.vargroup .variation').each(function() {
			var option 		= $(this).data('varoption');
			var group 		= $(this).parents('.row');
			var header 		= $(this).children('p').html();
			var description = $(this).data('varoptiondesc');
			if (variationsJson[option] != null) {
				$(this).addClass('has-options');
				$.each(variationsJson[option], function(k,v) {
					var image = "http://placehold.it/400x200?text=Mangler+billede";
					if (v[1] != "") {
						image = "/admin/public/getimage.ashx?Image="+v[1]+"&Format=jpg&Width=400&Height=200";
					}
					if ( $('.panel.panel-'+option).length <= 0) {
						var html = '<div class="panel panel-'+option+' text-center"><h3>'+header+'</h3>';
						if (description != "") {
							html += '<div class="container"><div class="col-xs-12 col-md-6 col-lg-8 col-md-push-3 col-lg-push-2"><p>'+description+'</p></div></div>';
						}
						html += '<div class="container variationlist"></div></div>';
						group.after(html);
					}					
					$('.panel.panel-'+option+' .variationlist').append('<div class="variation col-xs-4 col-md-3 col-lg-2"><img src="'+image+'" class="img-responsive" /><p>'+v[0]+'</p></div>')
				});
			};  
			
		});*/
		$.each(variationsJson, function(k,v){
			var description = v.GroupDescription;
			var option 		= v.GroupID; // $(this).data('varoption');
			if (option != "") {
				var currentVar  = $(".vargroup .variation[data-varoption="+option+"]");
				var group 		= currentVar.parents('.row');
				var header 		= currentVar.children('p').html();
				$.each(v.Variants, function(name, currentimage){
					var image = "http://placehold.it/400x200?text=Mangler+billede";
					if (currentimage != "") {
						image = "/admin/public/getimage.ashx?Image="+currentimage+"&Format=jpg&Width=400&Height=200";
					}
					if ( $('.panel.panel-'+option).length <= 0) {
						var html = '<div class="panel panel-'+option+' text-center"><h3>'+header+'</h3>';
						if (description != "") {
							html += '<div class="container"><div class="col-xs-12 col-md-6 col-lg-8 col-md-push-3 col-lg-push-2"><p>'+description+'</p></div></div>';
						}
						html += '<div class="container variationlist"></div></div>';
						group.after(html);
					}					
					$('.panel.panel-'+option+' .variationlist').append('<div class="variation col-xs-4 col-md-3 col-lg-2"><img src="'+image+'" class="img-responsive" /><p>'+name+'</p></div>')

				});
				currentVar.addClass('has-options');
			}
		});

	}

	$('.variation.has-options').click(function() {
		if (!$(this).hasClass("active")) {
			$('.variation').removeClass('active');
			$('.panel .panel').removeClass('active');
			var panel = $('.panel.panel-'+$(this).data('varoption'));
			if (panel.length > 0) {
				$(this).addClass('active');
				panel.addClass('active');
			}
		} else {
			$('.variation').removeClass('active');
			$('.panel .panel').removeClass('active');
		}
	}); 

	stickyPanels();

/*	$(window).resize(function() {
		stickyPanels();
	});*/

	$(window).scroll(function() {
	    clearTimeout($.data(this, 'scrollTimer'));
	    $.data(this, 'scrollTimer', setTimeout(function() {
	    	var limit = 100;
	    	var currentPos = Math.floor(window.scrollY);
	        $.each(panelPositions, function(k,v) {
	        	clearTimeout($.data(this, 'scrollTimer'));
	        	if ( currentPos+limit>v && (currentPos-limit/2)<v ) {
		        	$('html, body').animate({
			        	scrollTop: v
				    }, 50);
			    };
	        });
	    }, 250));
	});

	if ($('.page').first().hasClass('text-dark') || $('section').first().hasClass('text-dark')) {
		$('body').addClass('dark-logo');	
	}

});




function setModal(embed) {
	if ($('#modal').length < 1) {
		$('body').append('<div class="modal fade" id="modal" tabindex="-1" role="dialog"><div class="dialog" role="document"><div class="modal-content">'+
  	    embed+
  	    '</div></div></div>');
  	    $('#modal').modal('show');
	} else {
		$('#modal').modal('hide');
		$('#modal .modal-content').html(embed);
	    $('#modal').modal('show');
	}
}

function showContactForm(source) {
	source.preventDefault();
	if (!$(source.currentTarget).hasClass('active')){		
		$('.frontpage-form').toggleClass('active');
	}
	else {
		$('.frontpage-form').toggleClass('active');	
	}
	$(source.currentTarget).toggleClass('active');	
	
}

$(document).click(function(e){
		var target = e.target;

		if (!$(target).is('.menu-contact.hidden-xs') && !$(target).parents().is('.menu-contact.hidden-xs')) {
			if (!$(target).is('.frontpage-form') && !$(target).parents().is('.frontpage-form')) {
				$('.menu-contact.hidden-xs').removeClass('active');	
				$('.frontpage-form').removeClass('active');
			}
		}
});