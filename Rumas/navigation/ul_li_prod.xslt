<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" omit-xml-declaration="yes" indent="yes" encoding="utf-8" />
  <xsl:param name="html-content-type" />
  <xsl:template match="/NavigationTree">
    <ul>
      <xsl:apply-templates select="Page">
        <xsl:with-param name="depth" select="1" />
      </xsl:apply-templates>
    </ul>
  </xsl:template>
  <xsl:template match="Page">
    <xsl:param name="depth" />
    <xsl:if test="not(position() mod 4)">
    
    </xsl:if>
    <xsl:if test="@ID='9'">
    <li>
        <xsl:attribute name="class">col-xs-6 col-sm-3</xsl:attribute>
        <xsl:if test="@InPath='True'">
          <xsl:attribute name="class">col-xs-6 col-sm-3 inpath</xsl:attribute>
        </xsl:if>
        <xsl:if test="@Active='True'">
          <xsl:attribute name="class">col-xs-6 col-sm-3 active</xsl:attribute>
        </xsl:if>
        <xsl:if test="$depth=1">
             <xsl:attribute name="class">row</xsl:attribute>
       </xsl:if>
         <xsl:if test="$depth>2">
             <xsl:attribute name="class">col-xs-12</xsl:attribute>
       </xsl:if>
      <a class="level{@AbsoluteLevel}">
        <xsl:if test="@InPath='True'">
          <xsl:attribute name="class">inpath level<xsl:value-of select="@AbsoluteLevel" disable-output-escaping="yes" /></xsl:attribute>
        </xsl:if>
        <xsl:if test="@Active='True'">
          <xsl:attribute name="class">active level<xsl:value-of select="@AbsoluteLevel" disable-output-escaping="yes" /></xsl:attribute>
        </xsl:if>
        <xsl:attribute name="href">
            <xsl:value-of select="@FriendlyHref" disable-output-escaping="yes"/>
          </xsl:attribute>
        <xsl:if test="@Allowclick='True'"></xsl:if>
        <xsl:value-of select="@MenuText" disable-output-escaping="yes"/>
      </a>
      <xsl:if test="count(Page)">
        <ul class="level{@AbsoluteLevel}">
          <xsl:apply-templates select="Page">
            <xsl:with-param name="depth" select="$depth+1"/>
          </xsl:apply-templates>
        </ul>
      </xsl:if>
    </li>
   </xsl:if>
  </xsl:template>
</xsl:stylesheet>